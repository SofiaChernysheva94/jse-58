package ru.t1.chernysheva.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.api.repository.model.ITaskRepository;
import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    protected Class<Task> getEntityClass() {
        return Task.class;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(query, getEntityClass()).setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

}

package ru.t1.chernysheva.tm.repository.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.chernysheva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.chernysheva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.api.service.dto.IUserDtoService;
import ru.t1.chernysheva.tm.configuration.ServerConfiguration;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;
import ru.t1.chernysheva.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;
import java.util.*;

import static ru.t1.chernysheva.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskDtoRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private ITaskDtoRepository repository;

    @NotNull
    private IProjectDtoRepository projectRepository;

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private ProjectDTO project;

    @NotNull
    private ProjectDTO second_project;

    @NotNull
    private final IPropertyService propertyService = context.getBean(IPropertyService.class);

    @NotNull
    private final IUserDtoService userService = context.getBean(IUserDtoService.class);

    @Nullable
    private static String USER_ID_1;

    @Nullable
    private static String USER_ID_2;

    private static long USER_ID_COUNTER;

    @Nullable
    private static EntityManager ENTITY_MANAGER;

    @Nullable
    private static EntityManager ENTITY_MANAGER_PROJECT;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_ID_1 = userService.create("task_rep_usr_1_" + USER_ID_COUNTER, "1").getId();
        USER_ID_2 = userService.create("task_rep_usr_2_" + USER_ID_COUNTER, "1").getId();
        repository = context.getBean(ITaskDtoRepository.class);
        projectRepository = context.getBean(IProjectDtoRepository.class);
        taskList = new ArrayList<>();
        project = new ProjectDTO();
        project.setName("Test_project");
        project.setUserId(USER_ID_1);
        ENTITY_MANAGER = repository.getEntityManager();
        ENTITY_MANAGER_PROJECT = projectRepository.getEntityManager();
        ENTITY_MANAGER.getTransaction().begin();
        ENTITY_MANAGER_PROJECT.getTransaction().begin();
        projectRepository.add(USER_ID_1, project);
        ENTITY_MANAGER_PROJECT.getTransaction().commit();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setProjectId(project.getId());
            task.setUserId(USER_ID_1);
            repository.add(USER_ID_1, task);
            taskList.add(task);
        }
        second_project = new ProjectDTO();
        second_project.setName("Test_project_2");
        second_project.setUserId(USER_ID_2);
        ENTITY_MANAGER_PROJECT.getTransaction().begin();
        projectRepository.add(USER_ID_2, second_project);
        ENTITY_MANAGER_PROJECT.getTransaction().commit();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setProjectId(second_project.getId());
            task.setUserId(USER_ID_2);
            repository.add(USER_ID_2, task);
            taskList.add(task);
        }
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
    }

    @After
    public void clearAfter() {
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        repository.clear(USER_ID_1);
        repository.clear(USER_ID_2);
        projectRepository.clear(USER_ID_1);
        ENTITY_MANAGER.getTransaction().commit();
    }

    @AfterClass
    public static void closeConnection() {
        ENTITY_MANAGER.close();
    }

    @Test
    public void testAddTaskPositive() {
        TaskDTO task = new TaskDTO();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        repository.add(USER_ID_1, task);
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final TaskDTO task : taskList) {
            final TaskDTO foundTask = repository.findOneById(task.getUserId(), task.getId());
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(task.getId(), foundTask.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final TaskDTO task : taskList) {
            Assert.assertTrue(repository.existsById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final TaskDTO foundTask = repository.findOneByIndex(USER_ID_1, i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i).getId(), foundTask.getId());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final TaskDTO foundTask = repository.findOneByIndex(USER_ID_2, i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i + 5).getId(), foundTask.getId());
        }
    }

    @Test
    public void testFindAll() {
        @NotNull List<TaskDTO> tasks = repository.findAll(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(tasks.get(i).getId(), taskList.get(i).getId());
        }
        tasks = repository.findAll(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(tasks.get(i - 5).getId(), taskList.get(i).getId());
        }
    }

    @Test
    public void testFindAllOrderCreated() {
        List<TaskDTO> tasks = repository.findAll(USER_ID_1, CREATED_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_ID_2, CREATED_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderStatus() {
        List<TaskDTO> tasks = repository.findAll(USER_ID_1, STATUS_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_ID_2, STATUS_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderName() {
        List<TaskDTO> tasks = repository.findAll(USER_ID_1, NAME_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_ID_2, NAME_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.removeById(USER_ID_1, taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_1, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.removeById(USER_ID_2, taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_2, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.remove(USER_ID_1, taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.remove(USER_ID_2, taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testTaskFindByProjectId() {
        List<TaskDTO> tasks = repository.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
    }

}

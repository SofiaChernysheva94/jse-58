package ru.t1.chernysheva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.dto.request.TaskUpdateByIdRequest;
import ru.t1.chernysheva.tm.event.ConsoleEvent;
import ru.t1.chernysheva.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Update task by id.";

    @NotNull
    public static final String NAME = "task-update-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);

        taskEndpoint.updateTaskById(request);
    }

}

package ru.t1.chernysheva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.event.ConsoleEvent;
import ru.t1.chernysheva.tm.listener.AbstractListener;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Display list of terminal command.";

    @NotNull
    public static final String NAME = "help";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener.getArgument() != null && !listener.getArgument().isEmpty())
                System.out.println(listener.getArgument() + " - " + listener.getDescription());
        }
        for (@Nullable final AbstractListener listener : listeners) {
            System.out.println(listener.getName() + " - " + listener.getDescription());
        }

    }

}
